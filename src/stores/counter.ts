import { ref } from "vue";
import { defineStore } from "pinia";

export const useCounterStore = defineStore("counter", () => {
  const count = ref(0);
  const Double = ref(0)
  function increment() {
    count.value++;
    Double.value = count.value * 2;
  }
  function decrease() {
    if (count.value != 0) {
      count.value--;
      Double.value -= 2;
    }
  }

  return { count, increment, decrease, Double };
});
